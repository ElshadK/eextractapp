import React, { Component } from "react";
import {
  TouchableOpacity,
  View,
  Text,
  ScrollView,
  Dimensions,
  Image,
} from "react-native";
import * as AsyncStorage from "./../../Utils/Storage";

export default class index extends Component {
  state = { extracts: [] };

  componentDidMount() {
    this.GetExtracts();
  }

  GetExtracts = async () => {
    let extracts = await AsyncStorage.GetFromLocal();
    this.setState({ extracts: JSON.parse(extracts) });
  };

  DeleteExtract = async (extract) => {
    await AsyncStorage.RemoveFromLocal(extract);
    await this.GetExtracts();
  };

  render() {
    return (
      <View
        style={{
          backgroundColor: "#fafafa",
          height: "100%",
          // justifyContent: "center",
          flex: 1,
        }}
      >
        <ScrollView>
          {this.state.extracts.length > 0 ? (
            this.state.extracts.map((extract, i) => (
              <TouchableOpacity
                key={i}
                onPress={() =>
                  this.props.navigation.navigate("ShowExtract", {
                    url: extract.url,
                  })
                }
                style={{
                  width: "90%",
                  backgroundColor: "#fafafa",
                  borderRadius: 10,
                  elevation: 3,
                  margin: 10,
                  padding: 10,
                  alignSelf: "center",
                }}
              >
                <TouchableOpacity
                  onPress={() => this.DeleteExtract(extract)}
                  style={{
                    zIndex: 99999999,
                    position: "absolute",
                    padding: 5,
                    alignSelf: "flex-end",
                  }}
                >
                  <Image
                    style={{
                      width: 25,
                      height: 25,
                    }}
                    source={require("./../../images/del.png")}
                  />
                </TouchableOpacity>

                <Text style={{ fontSize: 18, fontWeight: "bold" }}>
                  Elektron çıxarış
                </Text>
                <View>
                  <Text style={{ margin: 2 }}>
                    Reyestr nömrəsi: {extract.reyestr}
                  </Text>
                  <Text style={{ margin: 2 }}>
                    Qeydiyyat nömrəsi: {extract.register}
                  </Text>
                </View>
              </TouchableOpacity>
            ))
          ) : (
            <View>
              <Image
                style={
                  {
                    // width: 25,
                    // height: 25,
                  }
                }
                source={require("./../../images/empty.jpg")}
              />
              <Text
                style={{
                  fontSize: 20,
                  justifyContent: "center",
                  textAlign: "center",
                }}
              >
                Elektron çıxarış əlavə etməmisiniz.
              </Text>
            </View>
          )}
        </ScrollView>
        <TouchableOpacity
          onPress={() => this.props.navigation.navigate("AddExtract")}
          style={{
            margin: 20,
            padding: 20,
            width: 70,
            height: 70,
            borderRadius: 70,
            backgroundColor: "#ff7e67",
            borderWidth: 1,
            borderColor: "#ff7e67",
            elevation: 5,
            justifyContent: "center",
            position: "absolute",
            left: Dimensions.get("window").width - 120,
            right: 0,
            bottom: 0,
          }}
        >
          <Text style={{ textAlign: "center", fontSize: 30, color: "#fafafa" }}>
            +
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}
