import React, { Component } from "react";
import { Share, TouchableOpacity, Image } from "react-native";
import { WebView } from "react-native-webview";


export default class index extends Component {
  componentDidMount() {
    this.props.navigation.setOptions({
      headerRight: () => (
        <TouchableOpacity onPress={async () => await this.onShare()}>
          <Image
            style={{ width: 30, height: 30, marginRight: 15 }}
            source={require("./../../images/share.png")}
          />
        </TouchableOpacity>
      ),
    });
  }

  onShare = async () => {
    try {
      const result = await Share.share({
        message: this.props.route.params.url,
        url: "https://images.unsplash.com/photo-1541963463532-d68292c34b19?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxleHBsb3JlLWZlZWR8M3x8fGVufDB8fHx8&w=1000&q=80",
      });
//      url: 'data:image/png;base64,<base64_data>'

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  };

   onMessage(data) { 
    console.log(data);
 }
  render() {
    const { url } = this.props.route.params;
    
//      document.getElementsByClassName("row")[0].style.display = "none";
//document.getElementById("btnPrintCertificate").style.display = "none";
    return (
      <WebView
      onNavigationStateChange={false}
      javaScriptEnabled={true}
      //injectedJavaScriptBeforeContentLoaded={true}
      injectedJavaScript={`
      window.ReactNativeWebView.postMessage(document.getElementById("lblReyestrNumberById").innerHTML+'~'+document.getElementById("lblRegistrNumberById").innerHTML);
      document.getElementById("btnPrintById").style.display = "none"; 
      document.getElementById("btnPrintById").style.margin = "50px"`}
        // style={{ width: Dimensions.get("window").width - 40 }}
        style={{ margin: 0 }}
        source={{
          uri: url, //'https://e-emlak.gov.az/eemdk/az/CheckElectronCertificate?id=180D6A6494FF44A2ADB5DD4E4BA68B9E',
        }}
        onMessage={
          event => console.log('Received: ', event.nativeEvent.data)
        }
      />
    );
  }
}
