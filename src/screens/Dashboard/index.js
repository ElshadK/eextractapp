import React, { Component } from "react";
import { Button, Image } from "react-native";
import ExtractList from "./../ExctractList/index";
import AddExtract from "./../AddExtract/index";
import ShowExtract from "./../ShowExtract/index";

import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { TouchableOpacity } from "react-native-gesture-handler";

export default class index extends Component {
  render() {
    const Stack = createStackNavigator();

    return (
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen
            name="ExtractList"
            component={ExtractList}
            options={{
              title: "Elektron Çıxarışlarım",
              headerLeft: () => null,
              headerTitleStyle: {
                color: "white",
                textAlign: "center",
                fontSize: 23,
              },
              headerStyle: {
                backgroundColor: "#07689f",
              },
            }}
          />
          <Stack.Screen
            name="AddExtract"
            component={AddExtract}
            options={{
              title: "Çıxarışın əlavə edilməsi",
              headerTitleStyle: {
                color: "white",
                textAlign: "center",
                fontSize: 23,
                marginRight: "auto",
              },
              headerStyle: {
                backgroundColor: "#07689f",
              },
              headerTintColor: "#fafafa",
            }}
          />
          <Stack.Screen
            name="ShowExtract"
            component={ShowExtract}
            options={{
              title: "Elektron çıxarış",
              headerTitleStyle: {
                color: "white",
                textAlign: "center",
                fontSize: 23,
                marginRight: "auto",
                marginLeft: "auto",
              },
              headerStyle: {
                backgroundColor: "#07689f",
              },
              headerTintColor: "#fafafa",
            }}
          />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}
