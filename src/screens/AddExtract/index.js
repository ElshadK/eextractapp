import React, { Component } from "react";
import {
  TouchableOpacity,
  View,
  Text,
  Modal,
  StyleSheet,
  TextInput,
  Dimensions,
  Alert,
} from "react-native";
import * as AsyncStorage from "./../../Utils/Storage";

import QRCodeScanner from "react-native-qrcode-scanner";

import MD5 from "MD5";

export default class index extends Component {
  state = {
    isRegistrAndReyestrModal: false,
    isQRModal: false,
    register: "",
    reyestr: "",
    key: "",
  };

  onSuccess = async (e) => {
    this.setModalVisible("isQRModal", !this.state.isQRModal);
    this.addExtract(e.data);
  };

  setModalVisible = (name, value) => {
    this.setState({ [name]: value });
  };

  handleChange = (name, value) => {
    this.setState({ [name]: value });
  };

  addExtract = async (url) => {
    let baseurl='https://e-emlak.gov.az/eemdk/az/CheckElectronExtract/qr?';
    let extract = {
      reyestr:
        url === ""
          ? this.state.reyestr
          : url.substring(url.indexOf("r=") + 2, url.indexOf("&q=")),
      register:
        url === ""
          ? this.state.register
          : url.substring(url.indexOf("q=") + 2, url.indexOf("&t=")),
      key: url === "" ? "" : url.substring(url.indexOf("t=") + 2, url.length),
      url: url,
    };
    extract.url==="" ? extract.url = baseurl + 'r=' + extract.reyestr + '&q=' + extract.register + '&t=' + extract.key: null;
    let checkExtract = await AsyncStorage.CheckExtract(extract);
    if (checkExtract) {
      Alert.alert(
        "Bu reyestr və qeydiyyat nömrəsinə uyğun çıxarış əlavə etmisiniz."
      );
    } else {
      await AsyncStorage.AddToLocal(extract);
      this.setModalVisible("isRegistrAndReyestrModal", false);
      this.props.navigation.push("ExtractList");
    }
  };

  render() {
    return (
      <View
        style={{
          backgroundColor: "#fafafa",
          height: "100%",
          justifyContent: "center",
        }}
      >
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.isRegistrAndReyestrModal}
          onRequestClose={() => {
            this.setModalVisible(
              "isRegistrAndReyestrModal",
              !this.state.isRegistrAndReyestrModal
            );
          }}
        >
          <View style={styles.centeredView}>
            <View style={styles.modalView}>
              <TextInput
                style={styles.input}
                placeholder="Reyestr nömrəsi"
                onChangeText={(value) => this.handleChange("reyestr", value)}
              />
              <TextInput
                style={styles.input}
                placeholder="Qeydiyyat nömrəsi"
                onChangeText={(value) => this.handleChange("register", value)}
              />
              <View style={{ flexDirection: "row" }}>
                <TouchableOpacity
                  onPress={() => this.addExtract("")}
                  style={styles.btnAdd}
                >
                  <Text style={styles.btnText}>Əlavə et</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() =>
                    this.setModalVisible(
                      "isRegistrAndReyestrModal",
                      !this.state.isRegistrAndReyestrModal
                    )
                  }
                  style={styles.btnCancel}
                >
                  <Text style={styles.btnText}>Bağla</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>

        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.isQRModal}
          onRequestClose={() => {
            this.setModalVisible("isQRModal", !this.state.isQRModal);
          }}
        >
          <View style={styles.centeredView}>
            <View style={styles.modalView}>
              <QRCodeScanner onRead={this.onSuccess} />
              <TouchableOpacity
                onPress={() =>
                  this.setModalVisible("isQRModal", !this.state.isQRModal)
                }
                style={styles.btnCancel}
              >
                <Text style={styles.btnText}>Bağla</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>

        <TouchableOpacity
          onPress={() =>
            this.setModalVisible(
              "isRegistrAndReyestrModal",
              !this.state.isRegistrAndReyestrModal
            )
          }
          style={styles.button}
        >
          <Text style={styles.buttonText}>
            Reyestr və qeydiyyat nömrəsi ilə
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() =>
            this.setModalVisible("isQRModal", !this.state.isQRModal)
          }
        >
          <Text style={styles.buttonText}>QR kod ilə</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 40,
  },
  modalView: {
    margin: 70,
    backgroundColor: "white",
    borderRadius: 10,
    padding: 10,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  input: {
    borderColor: "#07689f",
    borderWidth: 1,
    borderRadius: 10,
    margin: 5,
    width: Dimensions.get("window").width - 100,
  },
  btnAdd: {
    borderRadius: 10,
    backgroundColor: "#07689f",
    margin: 5,
    padding: 10,
  },
  btnCancel: {
    borderRadius: 10,
    backgroundColor: "darkred",
    margin: 5,
    padding: 10,
  },
  btnText: { color: "#fafafa", fontWeight: "bold", fontSize: 15 },
  button: {
    width: "90%",
    height: "30%",
    backgroundColor: "#07689f",
    borderColor: "white",
    borderRadius: 10,
    elevation: 3,
    margin: 10,
    alignItems: "center",
    alignSelf: "center",
    justifyContent: "center",
  },
  buttonText: {
    fontSize: 25,
    textAlign: "center",
    color: "#fff",
    padding: 10,
  },
});
