import AsyncStorage from "@react-native-async-storage/async-storage";
//import * as AXIOS from './../api/Axios';

export async function GetFromLocal() {
  try {
    const value = await AsyncStorage.getItem("@extracts");
    if (value !== null) {
      return value;
    } else return "";
  } catch (error) {
    return "";
  }
}

export async function AddToLocal(extract) {
  try {
    let data = await GetFromLocal();
    if (data == "") {
      await AsyncStorage.setItem("@extracts", JSON.stringify([extract]));
    } else {
      let extracts = JSON.parse(data);
      data = [...extracts, extract];
      await AsyncStorage.setItem("@extracts", JSON.stringify(data));
    }
  } catch (error) {
    console.log(error);
  }
}

export async function RemoveFromLocal(extract) {
  try {
    let data = await GetFromLocal();
    if (data != "") {
      let extracts = JSON.parse(data);
      data = extracts.filter(
        (e) => e.register != extract.register && e.reyestr != extract.reyestr
      );
      console.log(data);
      await AsyncStorage.setItem("@extracts", JSON.stringify(data));
    }
  } catch (error) {
    console.log(error);
  }
}

export async function RemoveFromLocalAll() {
  try {
    await AsyncStorage.removeItem("@extracts");
  } catch (error) {
    console.log(error);
  }
}

export async function CheckExtract(extract) {
  try {
    let data = await GetFromLocal();
    if (data != "") {
      let extracts = JSON.parse(data);
      data = extracts.filter(
        (e) => e.register == extract.register && e.reyestr == extract.reyestr
      );
      return data.length > 0 ? true : false;
    }
    return false;
  } catch (error) {
    console.log(error);
  }
}
