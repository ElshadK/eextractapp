import { StatusBar } from "expo-status-bar";
import React from "react";
import Dashboard from "./src/screens/Dashboard/index";

export default function App() {
  return <Dashboard />;
}
